package com.service.usermanagement.enums;

public enum UserRoles {
    ROLE_OFFICE, ROLE_ADMIN, ROLE_USER
}
