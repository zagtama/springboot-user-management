package com.service.usermanagement.models.account;

import com.service.usermanagement.enums.UserRoles;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "accounts", uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class Account implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID, generator = "uuid1")
    private String id;

    private LocalDateTime createTime;

    private String creatorName;

    private LocalDateTime updateTime;

    private String updaterName;

    private LocalDateTime deleteTime;

    private String deleterName;

    private boolean deleteStatus = false;

    @NotBlank
    @NotNull
    @Size(max = 30)
    private String name;

    @Size(max = 50)
    @Email
    private String email;

    @Enumerated(EnumType.STRING)
    private UserRoles roles = UserRoles.ROLE_USER;

    private String password;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(roles.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
