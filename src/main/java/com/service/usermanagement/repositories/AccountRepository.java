package com.service.usermanagement.repositories;

import com.service.usermanagement.models.account.Account;
import com.service.usermanagement.services.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, String>, AccountService {

    Optional<Account> findByEmail(String email);

    @Query(
            value = "SELECT * FROM accounts WHERE delete_status = false " +
                    "AND CASE WHEN char_length( :name ) >= 3 then name LIKE %:name% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :email ) >= 3 then email LIKE %:email% ELSE 1=1 END",
            countQuery = "SELECT COUNT(*) FROM accounts WHERE delete_status = false " +
                    "AND CASE WHEN char_length( :name ) >= 3 then name LIKE %:name% ELSE 1=1 END " +
                    "AND CASE WHEN char_length( :email ) >= 3 then email LIKE %:email% ELSE 1=1 END",
            nativeQuery = true)
    Page<Account> findAll(String name, String email, Pageable pageable);
}
