package com.service.usermanagement.services.impl;

import com.service.usermanagement.config.JwtService;
import com.service.usermanagement.models.account.Account;
import com.service.usermanagement.models.account.AccountRequest;
import com.service.usermanagement.models.auth.AuthenticationRequest;
import com.service.usermanagement.models.auth.AuthenticationResponse;
import com.service.usermanagement.repositories.AccountRepository;
import com.service.usermanagement.services.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private final AccountRepository accountRepository;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final JwtService jwtService;

    @Autowired
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(AccountRequest accountRequest) {
        Account account = new Account();
        account.setCreateTime(LocalDateTime.now());
        account.setCreatorName("SYSTEM");
        account.setName(accountRequest.getName());
        account.setEmail(accountRequest.getEmail());
        account.setPassword(passwordEncoder.encode(accountRequest.getPassword()));
        account.setRoles(accountRequest.getRoles());

        accountRepository.save(account);

        String token = jwtService.generateToken(account);
        return new AuthenticationResponse(token);

    }

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(), authenticationRequest.getPassword()));

        Account account = accountRepository.findByEmail(authenticationRequest.getEmail()).orElseThrow();

        String token = jwtService.generateToken(account);
        return new AuthenticationResponse(token);
    }
}
