package com.service.usermanagement.controllers;

import com.service.usermanagement.models.account.AccountRequest;
import com.service.usermanagement.models.auth.AuthenticationRequest;
import com.service.usermanagement.models.auth.AuthenticationResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @PostMapping("/register")
    public AuthenticationResponse register(@RequestBody AccountRequest accountRequest) {
        return null;
    }

    @PostMapping("/authenticate")
    public AuthenticationResponse login(@RequestBody AuthenticationRequest authenticationRequest) {
        return null;
    }


}
