package com.service.usermanagement.controllers;

import com.service.usermanagement.enums.UserRoles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/account/util")
public class AccountUtilController {

    @GetMapping("/combo/roles")
    public List<UserRoles> getAllRoles() {
        return Arrays.asList(UserRoles.values());
    }
}
