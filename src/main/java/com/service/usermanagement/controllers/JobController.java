package com.service.usermanagement.controllers;

import com.service.usermanagement.models.job.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/job")
public class JobController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/all")
    public Job[] getAllJob() {
        String url = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
        ResponseEntity<Job[]> responseEntity = restTemplate.getForEntity(url, Job[].class);
        return responseEntity.getBody();
    }

    @GetMapping("/{jobId}")
    public Job getJob(@PathVariable String jobId) {
        String url = "http://dev3.dansmultipro.co.id/api/recruitment/positions/" + jobId;
        return restTemplate.getForObject(url, Job.class);
    }
}
