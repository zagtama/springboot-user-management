package com.service.usermanagement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.service.usermanagement.models.account.Account;
import com.service.usermanagement.models.account.AccountRequest;
import com.service.usermanagement.repositories.AccountRepository;
import com.service.usermanagement.utils.exception.BadRequestException;
import com.service.usermanagement.utils.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;


@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/all")
    public Page<Account> getAllAccount(@RequestParam(required = false, value = "") String name,
                                       @RequestParam(required = false, value = "") String email,
                                       Pageable pageable) {
        return accountRepository.findAll(name, email, pageable);
    }

    @GetMapping("/{accountId}")
    public Account getAccount(@PathVariable String accountId) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Account data not found on the system. ID: " + accountId);
        }

        return accountOptional.get();
    }

    @PutMapping("/{accountId}")
    public Account editAccount(@PathVariable String accountId, @RequestBody AccountRequest accountRequest) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Account data not found on the system. ID: " + accountId);
        }

        Account accountData;
        accountData = accountRepository.findByEmail(accountRequest.getEmail().trim().toLowerCase()).orElseThrow();
        if (!accountData.getEmail().equals(accountRequest.getEmail().trim().toLowerCase())) {
            throw new BadRequestException("Email already registered in the system. Email: " + accountRequest.getEmail());
        }

        Account account = accountOptional.get();
        account.setUpdateTime(LocalDateTime.now());
        account.setUpdaterName("SYSTEM");
        account.setName(accountRequest.getName());
        account.setEmail(accountRequest.getEmail());
        account.setPassword(accountRequest.getPassword());
        account.setRoles(accountRequest.getRoles());

        accountRepository.save(account);

        return account;
    }

    @DeleteMapping("/{accountId}")
    public ObjectNode deleteAccount(@PathVariable String accountId) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if (accountOptional.isEmpty()) {
            throw new NotFoundException("Data akun tidak ditemukan. ID: " + accountId);
        }

        Account account = accountOptional.get();
        account.setDeleteTime(LocalDateTime.now());
        account.setDeleterName("SYSTEM");
        account.setDeleteStatus(true);

        accountRepository.save(account);

        ObjectNode response = objectMapper.createObjectNode();
        response.put("message", "Success delete account. ID: " + accountId);

        return response;
    }
}
